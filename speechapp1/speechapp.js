/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*jslint node: true, white: true, browser: true */
/*jslint global webkitSpeechRecognition */
"use strict";

//global variables
var recognition; 
var speechButton, statusElement, transcript, speechResult;
var recognitionEvent;
var subResultTranscript = new Array();
var tempPair;
var lives = 10;
var score = 0;
var resultTranscript;
//hard coded pairs
/*var minimalPairs = [['pin', 'bin'], ['rot', 'lot'], ['bin', 'bean'], ['pen', 'pan'], ['hat', 'had'], ['cat', 'bat'], ['wide', 'wise'], ['ride', 'road'], 
	['kite', 'night'], ['take', 'steak'], ['sit', 'seat'], ['desk', 'disk'], ['wet', 'wait'], ['bad', 'bed'], ['so', 'saw'], ['not', 'note'], ['fast', 'first'], 
	['berry', 'very'], ['pie', 'buy'], ['thin', 'thing'], ['alive', 'arrive'], ['catch', 'cat'], ['sea', 'she'], ['fan', 'van'], ['fat', 'hat'], 
	['sink', 'think'], ['with', 'whizz'], ['fast', 'past'], ['came', 'game'], ['two', 'do'], ['cat', 'cut'], ['ankle', 'uncle'], ['ran', 'run'],
	['drank', 'drunk'], ['match', 'much'], ['ship', 'sheep'], ['it', 'eat'], ['hit', 'heat'], ['live', 'leave'], ['bat', 'bed'], ['pack', 'peck'],
	['sad', 'said'],['tan', 'ten'], ['work', 'walk'], ['hat', 'hate'], ['mad', 'made'], ['lack', 'lake'], ['back', 'bake'], ['cap', 'cape'], 
	['far', 'four'], ['part', 'port'], ['tart', 'taught'], ['farm', 'form'], ['barn', 'born'], ['ship', 'sip'], ['bird', 'bored'], ['fur', 'for'], 
	['shirt', 'short'], ['sir', 'saw'], ['sure', 'sore'], ['shoot', 'suit'], ['shy', 'sigh'], ['sip', 'zip'], ['sue', 'zoo'], ['place', 'play'], 
	['rice', 'rise'], ['ice', 'eyes'], ['shoes', 'choose'], ['sheep', 'cheap'], ['wash', 'watch'], ['cash', 'catch']
];*/
var minimalPairs = [];
var pair = generatePair();
var scoreArr = [];



//a random generator which returns a pair of words from the list provided
function generatePair(){
	var randomPair;
	var pairWords;
	if (minimalPairs.length < 1){
		minimalPairs = [['pin', 'bin'],	['rot', 'lot'], ['bin', 'bean']];
		randomPair = minimalPairs[Math.floor(Math.random() * minimalPairs.length)];
		pairWords = randomPair[0] + " " + randomPair[1];
	} else {
		console.log("Txt File loaded");
		randomPair = minimalPairs[Math.floor(Math.random() * minimalPairs.length)];
		pairWords = randomPair[0] + " " + randomPair[1];
	}
	return pairWords;
}


//function which is called when the game is over
function gameOver() {
	alert('Game Over! ' + 'Your Score is: ' + score);
	location.reload();
	
}


//function which removes vowels and plurals
function fixStrings(arr){
	var newArray = new Array();
	for (var i = 0; i <arr.length; i++){
		newArray[i] = arr[i].toString().toLowerCase().replace(/[aeiouy]/gi, '');  
		if (newArray[i].endsWith("s")){ //check if string ends with char 's'
			newArray[i] = newArray[i].substring(0, newArray[i].length - 1); // return new string with last character cut off
		}
	}
	return newArray;
}

//function which deals with spanish/portugese and chinese speakers - (not used in game)
//see "Accent Reduction" and "H-dropping" on wikipedia 
function fixStringsAccent(arr){
	var newArray = new Array();
	for (var i = 0; i <arr.length; i++){
		if (newArray[i].endsWith("is") || endsWith("it") || endsWith("ill")){
			newArray[i] = h.concat(newArray[i]);
		} else if (newArray[i].endsWith("in")){
			newArray[i] = "y".concat(newArray[i]);
		} else if  (newArray[i].startsWith("h")){
			newArray[i] = newArray[i].substr(1); //removes first character in string if begins with H
		}
	}
	return newArray;
}

// unfinished levenschtein algorithm
/*function levenschteinDistance(a, b) {
	var lArray = new Array();
	//iterate through first word
	for (var i = 0; i <a.length; i++){
		lArray[i] = [i];
	}	
	
	//iterate through second word
	for (var j = 0; j <b.length; j++){
		 lArray[j] = [j];
	}
	
	//look for the numebr of differences
	for ( i = 1; i <a.length; i++){

			if(a.charAt(i-1){
				lArray[i] = lArray[i-1];
			} else if (a.charAt(i-2){
				lArray[i] = lArray[i-2];
			} else if(a.charAt(i-3){
				lArray[i] = lArray[i-3];
			} else if (a.charAt(i-4){
				lArray[i] = lArray[i-4];
			} else if (a.charAt(i-5){
				lArray[i] = lArray[i-5];
			}
	}
	for (j=1; j< b.length; j++){
		if(b.charAt(j-1){
				lArray[i] = lArray[j-1];
			} else if (b.charAt(j-2){
				lArray[j] = lArray[j-2];
			} else if(b.charAt(j-3){
				lArray[j] = lArray[j-3];
			} else if (b.charAt(i-4){
				lArray[j] = lArray[j-4];
			} else if (b.charAt(j-5){
				lArray[j] = lArray[j-5];
			}
	}
	return lArray[a][b];
}*/

//function which makes the "Speak Now" message flash
function textBlink(textColor, colorChange){
	var tempColor = document.getElementById("speech_status_text").style.color;
	if (tempColor === 'black'){
		document.getElementById("speech_status_text").style.color = colorChange;
	} else {
		document.getElementById("speech_status_text").style.color = 'black';
	}
}

//function which makes the "Please Wait" message flash
function loadScreenTextBlink(textColor, colorChange){
	var tempColor = document.getElementById("pleasewait").style.color;
	if (tempColor === 'black'){
		document.getElementById("pleasewait").style.color = colorChange;
	} else {
		document.getElementById("pleasewait").style.color = 'black';
	}
}


//ajax function to load data from txt file to js array
function loadTxtFile(){
	var load = new XMLHttpRequest();
	load.onreadystatechange=function(){
		if (load.readyState==4 && load.status >= 200 && load.status < 300 || (load.status == 304)){ //check if server is responding
				var tempString2 = load.responseText;
				var lines = tempString2.split("\n"); //split txt file by each line to get pairs of words
				var mergeArray = function(arrToSplit, arrSize){ //split big array into small arrays
					var subArr = [], i;
					for (i = 0; i <arrToSplit.length; i+=1){
						subArr.push(arrToSplit.slice(i, i + arrSize)); 
				}
					return subArr;
				}
				var newArr = mergeArray(lines, 1);
				for (var v = 0; v < newArr.length; v++){
					newArr[v] = newArr[v].toString().split(" ");
				}
				
				minimalPairs = newArr;
				
		}  else {
				document.getElementById('ajax').innerHTML = "Error loading files: " + load.statusText;	
		}
	}
	load.open("GET", "minimalpairs.txt", true);
	load.send();
	
	return load;
}

//displays the last score
function scoreList(){
	document.getElementById("backbutton").style.visibility="visible";
	document.getElementById("scorelist").style.visibility="visible";
	document.getElementById("infobutton").style.visibility="hidden";
	document.getElementById("scoresbutton").style.visibility="hidden";
	document.getElementById("buttontostart").style.visibility="hidden";
	document.getElementById("playbutton").style.visibility="hidden";
	document.getElementById("gametitle").style.visibility="hidden";
	document.getElementById("scorelist").innerHTML = "Your last score is " +  JSON.parse(localStorage.getItem("scorelist1")); 
}



//called at the init() function to hide elements in the game 
function hideGame(){
	document.getElementById("heart1").style.visibility="hidden";
	document.getElementById("lives").style.visibility="hidden";
	document.getElementById("score").style.visibility="hidden";
	document.getElementById("speech_button").style.visibility="hidden";

}

//shows relevant elements at the start of the game
function showGame(){
	document.getElementById("heart1").style.visibility="visible";
	document.getElementById("lives").style.visibility="visible";
	document.getElementById("score").style.visibility="visible";
	document.getElementById("speech_button").style.visibility="visible";
	document.getElementById("homebutton").style.visibility="visible";
	document.getElementById("infobutton").style.visibility="hidden";
	document.getElementById("playbutton").style.visibility="hidden";
	document.getElementById("gametitle").style.visibility="hidden";
	document.getElementById("hourglass").style.visibility="hidden";
	document.getElementById("speech_output").style.visibility="visible";
	document.getElementById("speechresult").style.visibility="visible";
	document.getElementById("pleasewait").style.visibility="hidden";
	document.getElementById("buttontostart").style.visibility="hidden";

}

//takes the player back to the main menu.
function menuScreen(){
	location.reload();
	document.getElementById("infobutton").style.visibility="visible";
	document.getElementById("gametitle").style.visibility="visible";
	document.getElementById("playbutton").style.visibility="visible";
	document.getElementById("heart1").style.visibility="hidden";
	document.getElementById("lives").style.visibility="hidden";
	document.getElementById("score").style.visibility="hidden";
	document.getElementById("speech_button").style.visibility="hidden";
	document.getElementById("homebutton").style.visibility="hidden";
	document.getElementById("speech_output").style.visibility="hidden";
	document.getElementById("speechresult").style.visibility="hidden";
	document.getElementById("backbutton").style.visibility="hidden";
	
}

//displays the information menu
function infoMenu(){
	document.getElementById("infobutton").style.visibility="hidden";
	document.getElementById("gametitle").style.visibility="hidden";
	document.getElementById("playbutton").style.visibility="hidden";
	document.getElementById("scoresbutton").style.visibility="hidden";
}

//displays the relevant elements for the loading screen
function loadScreen(){
	document.getElementById("infobutton").style.visibility="hidden";
	document.getElementById("gametitle").style.visibility="hidden";
	document.getElementById("scoresbutton").style.visibility="hidden";
	document.getElementById("playbutton").style.visibility="hidden";
	document.getElementById("heart1").style.visibility="hidden";
	document.getElementById("lives").style.visibility="hidden";
	document.getElementById("score").style.visibility="hidden";
	document.getElementById("speech_button").style.visibility="hidden";
	document.getElementById("homebutton").style.visibility="hidden";
	document.getElementById("speech_output").style.visibility="hidden";
	document.getElementById("speechresult").style.visibility="hidden";
	document.getElementById("backbutton").style.visibility="hidden";
	document.getElementById("hourglass").style.visibility="visible";
	document.getElementById("pleasewait").style.visibility="visible";
	timerToGame();
}


//acts as 'loading screen' for ajax files to load
function timerToGame(){
	var sec = 5;
	function ticker() {
		var tick = document.getElementById("timerid");
		tick.style.visibility="hidden";
		sec--;
		tick.innerHTML = "0: " + (sec < 10 ? "0" : "") + String(sec);
		if (sec > 0){
			setTimeout(ticker, 1000);
		} else {
			tick.style.visibility = "hidden";
			countdownButtonOnly();
		}
	}
	ticker();	
}

//hides every HTML element apart from the start game button(pre-game button)
function countdownButtonOnly(){
	document.getElementById("hourglass").style.visibility="hidden";
	document.getElementById("pleasewait").style.visibility="hidden";
	document.getElementById("infobutton").style.visibility="hidden";
	document.getElementById("gametitle").style.visibility="hidden";
	document.getElementById("playbutton").style.visibility="hidden";
	document.getElementById("heart1").style.visibility="hidden";
	document.getElementById("lives").style.visibility="hidden";
	document.getElementById("score").style.visibility="hidden";
	document.getElementById("buttontostart").style.visibility="visible";
	document.getElementById("speech_button").style.visibility="hidden";
	document.getElementById("homebutton").style.visibility="hidden";
	document.getElementById("speech_output").style.visibility="hidden";
	document.getElementById("speechresult").style.visibility="hidden";
}

//timer which is called when the player starts the game.
function countdownToGame(){
	document.getElementById("buttontostart").style.visibility="hidden";
	document.getElementById("hourglass").style.visibility="hidden";
	document.getElementById("pleasewait").style.visibility="hidden";

	var s = 4;
	function ticker(){
		var tic = document.getElementById("gametimer");
		s--;
		tic.innerHTML = "0: " + (s < 10 ? "0" : "") + String(s);
		if (s > 0){
			setTimeout(ticker, 1000);
		} else {
			tic.style.visibility = "hidden";
			nextSet();	
			console.log("game started");
			console.log("pair of words generated");
		}
	}
	ticker();
}

//diplays the next pair of words and requests user for input
function nextSet(){
		setInterval(function() {
			textBlink('speech_status_text', 'red');
		}, 600);
		document.getElementById("heart1").style.visibility="visible";
		document.getElementById("lives").style.visibility="visible";
		document.getElementById("score").style.visibility="visible";
		document.getElementById("oldminpair").style.visibility='visible';
		document.getElementById("speech_status_text").innerHTML = "Speak Now!";
		document.getElementById("speech_button").style.visibility='hidden';
		document.getElementById("minpairs").style.visibility='visible';
        statusElement.innerHTML=document.getElementById("speech_status").style.visibility='visible' ;
		document.getElementById("speech_status_text").style.visibility='visible' ;
		document.getElementById("speechresult").style.visibility='hidden';

        recognition.start();
		console.log("recognition started");
			recognition.onerror = function(event){
			alert("I didn't hear anything from you.");
			console.log("Error: No input from user.")
			document.getElementById("speech_button").style.visibility='visible';
			document.getElementById("minpairs").style.visibility='hidden';
			document.getElementById("speechresult").style.visibility='hidden';
			document.getElementById("speech_status").style.visibility='hidden';
			document.getElementById("speech_status_text").style.visibility='hidden';
			}
			
			recognition.onsoundend = function(event){
				recognition.stop();
				document.getElementById("speech_button").style.visibility='visible';
				document.getElementById("minpairs").style.visibility='hidden';
				document.getElementById("speechresult").style.visibility='hidden';
				document.getElementById("speech_status").style.visibility='hidden';
				document.getElementById("speech_status_text").style.visibility='hidden';
				document.getElementById("speech_button").style.visibility="visible";
			}
			
}
	
//displays the game results after input is received by the recognizer
function gameResults(){
	recognition.onresult = function(event) { 
		if (event.results.length > 0){
		console.log("user results returned");
		document.getElementById("homebutton").style.visibility="visible";
		document.getElementById("speech_button").style.visibility="visible";
		document.getElementById("speechresult").style.visibility="visible";
		document.getElementById("speech_status").style.visibility="hidden";
		document.getElementById("speech_status_text").style.visibility="hidden";
		document.getElementById("oldminpair").style.visibility='visible';

		resultTranscript = event.results[0][0].transcript;
		subResultTranscript = resultTranscript.split(" ");
		subResultTranscript = fixStrings(subResultTranscript);
		console.log("Words received (excluding vowels) = " + subResultTranscript);
		console.log("Number of words received: " + subResultTranscript.length);
		tempPair = pair.split(" "); //splits the words into two by the space between the string
		tempPair = fixStrings(tempPair); //removing the vowels and plural from the string
		console.log("Pair of words generated (excluding vowels)= " + tempPair);
		console.log("Number of words generated from txt file: " + tempPair.length); 
		if (subResultTranscript.length < 2) { //error handling for less than two words received by recognizer
			console.log("Error: not enough words received");
			alert("You didn't say enough words! Please try again.");
			document.getElementById("speechresult").style.visibility='hidden';
			document.getElementById("speech_button").innerHTML = "try again";
		} else if (subResultTranscript.length > 2) { //error handling for more than two words received by the recognizer
			console.log("Error: too many words received");
			alert("You said too many words! Please try again.");
			document.getElementById("speechresult").style.visibility='hidden';
			document.getElementById("speech_button").innerHTML = "try again";
		} else {
		
		console.log("Temp pair > " +tempPair[0]+"-"+tempPair[1]);
		
		//main game logic to assign scores
		if (tempPair[0] == subResultTranscript[0] && tempPair[1] == subResultTranscript[1]) {
			score = score +2;
			document.getElementById("speechresult").innerHTML = '<img src="images/tick.png" width="50" height="50"/>' + '<img src="images/tick.png" width="50" height="50"/>' + "<br>" + "<br>" + "You said both words correctly." + "<br>" + " 2 Points!" + "<br>" + "Press 'Next' to continue.";
			document.getElementById("speech_output").style.visibility='visible';
			document.getElementById("minpairs").style.visibility='hidden';
			document.getElementById("score").innerHTML = "Score:" + score;	
			document.getElementById("speech_status").style.visibility='hidden';
		} else if (tempPair[0] == subResultTranscript[0] && tempPair[1] != subResultTranscript[1]) {
			score = score + 1;
			document.getElementById("speechresult").innerHTML = '<img src="images/tick.png" width="50" height="50"/>' + "<br>" + "<br>" + "You only said the first word correctly" + "<br>" + "1 Point!" + "<br>" + "Press 'Next' to continue.";
			document.getElementById("minpairs").style.visibility='hidden';
			document.getElementById("speech_output").style.visibility='visible';
			document.getElementById("score").innerHTML = "Score:" + score;	
			document.getElementById("speech_status").style.visibility='hidden';
		} else if (tempPair[0] != subResultTranscript[0] && tempPair[1] == subResultTranscript[1]) {
			score = score + 1;
			document.getElementById("speechresult").innerHTML = '<img src="images/tick.png" width="50" height="50"/>' + "<br>" + "<br>" +"You only said the second word correctly" + "<br>" + " 1 Point!" + "<br>" + "Press 'Next' to continue.";
			document.getElementById("minpairs").style.visibility='hidden';
			document.getElementById("speech_output").style.visibility='visible';
			document.getElementById("score").innerHTML = "Score:" + score;	
			document.getElementById("speech_status").style.visibility='hidden';
		} else if (tempPair[0] != subResultTranscript[0] && tempPair[1] != subResultTranscript[1]){
			document.getElementById("speechresult").innerHTML = '<img src="images/cross.png" width="50" height="50"/>' + "<br>" + "<br>" + "You didn't say any words correctly." ;
			document.getElementById("minpairs").style.visibility='hidden';
			document.getElementById("speech_output").style.visibility='visible';
			lives--;
			document.getElementById("speech_status").style.visibility='hidden';
			if (lives == 0){ //end the game if lives are 0
				scoreArr.push(score.toString());
				scoreArr.sort();
				localStorage.setItem("scorelist1", JSON.stringify(scoreArr)); //convert array to string as it is the only supported format for localStorage
				gameOver();
				
			}
			
			document.getElementById("lives").innerHTML = "x" + lives;
			
		}
		var oldPair = pair; 
		document.getElementById("oldminpair").innerHTML = "Last pair: " + "<br>"+ oldPair;		
		pair = generatePair(); //update the pair for the next round
		document.getElementById("minpairs").innerHTML = pair;
		document.getElementById("speech_button").innerHTML = "Next";
		document.getElementById("speech_status").style.visibility='hidden';
		document.getElementById("oldminpair").style.visibility='hidden';
			
		
        var resultConfidence = Math.round(100*event.results[0][0].confidence);
		statusElement.innerHTML="";
		console.log(event);
        console.log(recognition);
        transcript.innerHTML= "You said: " +"<br>" + resultTranscript;
		console.log("Words received: " + resultTranscript);
		console.log("Confidence: " + resultConfidence + "%");
		}
	}
		
	};
}

//called to initialise everything when application is first launched
function init() {
	//direct user to use google chrome if they're using any other web browser - web speech only supports Google Chrome!
	if (!(window.webkitSpeechRecognition)) {
        alert('Please use Google Chrome');
      } else {
		  
	loadTxtFile();
	hideGame();
	
	//declare and set up HTML elements on the page
	document.getElementById("minpairs").innerHTML = pair;
	speechButton = document.getElementById("speech_button");
    statusElement = document.getElementById("speech_status");
    transcript = document.getElementById("speech_output");
	speechResult = document.getElementById("speechresult");
	speechButton.style.visibility="hidden";
	var backbutton1 = document.getElementById("backbutton")
	backbutton1.style.visibility="hidden";
	document.getElementById("minpairs").style.visibility="hidden";
	var infoButton1 = document.getElementById("infobutton");
	infoButton1.innerHTML = '<img src="images/info.png" width="20" height="20"/>';
	var scoreButton1 = document.getElementById("scoresbutton");
	scoreButton1.innerHTML ='<img src="images/trophy.png" width="20" height="20"/>';
	var toGame = document.getElementById("playbutton");
	var home = document.getElementById("homebutton");
	home.innerHTML = '<img src="images/home.png" width="15" height="15"/>';
	var hourglass1 = document.getElementById("hourglass");
	hourglass1.innerHTML = '<img src="images/hourglass.png" width="200" height="200"/>';
	hourglass1.style.visibility="hidden";
	document.getElementById("pleasewait").style.visibility="hidden";
	document.getElementById("instructions").style.visibility="hidden";
	var buttontostart1 = document.getElementById("buttontostart");
	buttontostart1.style.visibility="hidden";
	
	//creating google's web speech object.
    recognition = new webkitSpeechRecognition();
    recognition.lang = "en-GB";
	
    // set the function to be called when we have results
    gameResults();

	// listeners to handle events when each button is clicked
	//main game button
    speechButton.addEventListener("click", function() {
		nextSet();
    });
		
	//button which triggers the loading screen
	toGame.addEventListener("click", function(){
		console.log("Play button pressed");
		loadScreen();
		setInterval(function() {
		loadScreenTextBlink('pleasewait', 'lightblue');
		}, 300);
	});
		
	
	//button for when the home button is clicked
	home.addEventListener("click", function(){
		alert('Game ended. Your final score is: ' + score + " ");
		scoreArr.push(score.toString());
		scoreArr.sort();
		localStorage.setItem("scorelist1", JSON.stringify(scoreArr));
		menuScreen();
		console.log("Home button pressed.");
	});
		
	
	//when info button is clicked
	infoButton1.addEventListener("click", function() {
		console.log("Info button pressed");
		document.getElementById("backbutton").style.visibility="visible";
		instructions.style.visibility="visible";
		infoButton1.style.visibility="hidden";
		document.getElementById("scoresbutton").style.visibility="hidden";
		toGame.style.visibility="hidden";
		document.getElementById("gametitle").style.visibility="hidden";	
	});
	
	//back button - used for info and score menus
	backbutton1.addEventListener("click", function(){
		console.log("back button pressed");
		menuScreen();
	});
		
	//a pre-game button, activates the actual game
	buttontostart1.addEventListener("click", function(){
		console.log("Start Game button pressed");
		countdownToGame();
	});
	
	scoreButton1.addEventListener("click", function(){
		console.log("Score button pressed");
		scoreList();
	});
		
	  }
}

window.addEventListener("load",init);